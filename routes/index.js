const Router = require('koa-router');
const router = new Router();

const main = require('./main');
const books = require('./books');

router.use('/', main.routes());
router.use('/books', books.routes());

module.exports = router;
