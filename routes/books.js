
const Router = require('koa-router');
const books = new Router();
const controller = require('./../controllers');

books.get('/', async (ctx, next) => {
  const allBooks = await controller.books.findAll(ctx.query);
  
  ctx.body = (allBooks && !allBooks.error) ?
    { success: true, books: allBooks} :
    { success: false, error: (allBooks && allBooks.error) ? allBooks.error: '' };
  await next();
});

books.get('/:id', async (ctx, next) => {
  const book = await controller.books.findById(ctx.params.id);
  
  ctx.body = (book && !book.error) ?
    { success: true, book} :
    { success: false, error: (book && book.error) ? book.error: '' };
  await next();
});

books.post('/', async (ctx, next) => {
  const book = await controller.books.create(ctx.request.body);
  ctx.body = (book && !book.error) ?
    { success: true, book } :
    { success: false, error: (book && book.error) ? book.error: '' };
  await next();
});

books.patch('/:id', async (ctx, next) => {
  let book = await controller.books.findById(ctx.params.id);
  let updatedBook = null;
  if (book) {
    let params = { ...ctx.request.body };
    params['id'] = ctx.params.id;
    updatedBook = await controller.books.update(params);
    book = await controller.books.findById(ctx.params.id);
  }
  ctx.body = (updatedBook && updatedBook.changedRows && !updatedBook.error && book) ?
    { success: true, book } :
    { success: false, error: (updatedBook && updatedBook.error) ? updatedBook.error : '' };
  await next();
});

books.delete('/:id', async (ctx, next) => {
  const book = await controller.books.findById(ctx.params.id);
  let deletedBook = null;
  if (book) {
    deletedBook = await controller.books.destroy(ctx.params.id);
  }
  ctx.body = (deletedBook && deletedBook.changedRows && !deletedBook.error) ?
    { success: true }
  : { success: false, error: (deletedBook && deletedBook.error) ? deletedBook.error: '' };
  await next();
});

module.exports = books;
