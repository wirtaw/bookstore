const Router = require('koa-router');
const main = new Router();

main.get('/', async (ctx, next) => {
  
  ctx.body = 'Welcome in the BookStore!!!';
  await next();
});

module.exports = main;
