const logger = require('koa-logger');
const router = require('./routes');
const bodyParser = require('koa-bodyparser');
const koaqs = require('koa-qs');

const Koa = require('koa');
const app = new Koa();



koaqs(app);

app
  .use(logger())
  .use(bodyParser())
  .use(router.routes())
  .use(router.allowedMethods());

function normalizePort(val) {
  const port = parseInt(val, 10);
  
  if (isNaN(port)) {
    return val;
  }
  
  if (port >= 0) {
    return port;
  }
  
  return false;
}

const server = app.listen(normalizePort(process.env.PORT || 3000));
module.exports = server;
