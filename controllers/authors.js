'use strict';

const findAll = () => {
  return null;
};

const findById = () => {
  return null;
};

const create = () => {
  return null;
};

const update = () => {
  return null;
};

const destroy = () => {
  return null;
};

module.exports = {
  findAll,
  findById,
  create,
  update,
  destroy
};
