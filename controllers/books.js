'use strict';
const pool = require('./../middleware/database');

const findAll = async (query = {}) => {
  try {
    let limit = '';
    let page = 1;
    let perPage = 10;
    let orderBy = '';
    let deleted = ' WHERE deleted_at IS NULL';
    let errors = 1;
    const orderList = ['title', 'description', 'date', 'pages', 'updated_at', 'created_at'];
    if (query.order && query.sort && orderList.includes(query.sort)) {
      let dir = (query.order && query.order.toUpperCase() === 'ASC') ? 'ASC' : 'DESC';
      orderBy = ` ORDER BY ${query.sort} ${dir}`;
    } else {
      errors = 0;
    }
    if (query.page && query.limit && !isNaN(parseInt(query.limit,10))
      && !isNaN(parseInt(query.page,10))) {
      perPage = parseInt(query.limit,10);
      page = parseInt(query.page,10);
      limit = ` LIMIT ${(page - 1)*perPage}, ${perPage}`;
    } else {
      errors = -1;
    }
    if (errors) {
      return await pool.query(`SELECT * FROM books${deleted}${orderBy}${limit}`);
    } else {
      if (errors === 0) {
        return { error: 'Wrong column title'};
      } else if (errors === -1) {
        return { error: 'Wrong page or per page params'};
      }
    }
    
  } catch(err) {
    return { error: err.toString()};
  }
};

const findById = async (id) => {
  if (id) {
    try {
      let deleted = ' AND deleted_at IS NULL';
      return await pool.query(`SELECT * FROM books WHERE id=${pool.escape(id)}${deleted}`);
    } catch(err) {
      return { error: err.toString()};
    }
  } else {
    return { error: 'Wrong book id'};
  }
};

const create = async (params = {}) => {
  let uuid = await pool.query(`SELECT UUID() as 'uuid';`);
  const book  = {
    id: (uuid && uuid[0] && uuid[0].uuid) ? uuid[0].uuid : null,
    title: params.title ? params.title : '',
    description: params.description ? params.description : '',
    created_at: new Date()
  };
  if (params.date) {
    book['date'] = new Date(params.date);
  }
  if (params.pages) {
    book['pages'] = parseInt(params.pages,10);
  }
  if (params.image) {
    book['image'] = params.image;
  }
  if (book.title && book.description && book.date && book.id) {
    let save = await pool.query(`INSERT INTO books SET ?`, book);
    if (save && save.affectedRows) {
      if (book.id) {
        return await pool.query(`SELECT * FROM books WHERE id=${pool.escape(book.id)}`);
      } else {
        return { error: 'Failed to get created book'};
      }
    } else {
      return { error: 'Failed to save book'};
    }
    
  } else {
    return { error: 'Empty book title or description or date'};
  }
};

const update = async (params = {}) => {
  const bookParams  = [];
  let bookTitles = '';
  if (params.title) {
    let comma = (bookParams.length === 0) ? ',' : '';
    bookTitles = bookTitles + ' title = ?' + comma;
    bookParams.push(params.title);
  }
  //console.log(`bookTitles1 = ${JSON.stringify(bookTitles)}`);
  if (params.description) {
    let comma = (bookParams.length > 0) ? ',' : '';
    bookTitles = bookTitles + ' description = ?' + comma;
    bookParams.push(params.description);
  }
  //console.log(`bookTitles2 = ${JSON.stringify(bookTitles)}`);
  if (params.date && !isNaN(parseInt(params.date))) {
    let comma = (bookParams.length > 0) ? ',' : '';
    bookTitles = bookTitles + ' date = ?' + comma;
    bookParams.push(parseInt(params.date));
  }
  //console.log(`bookTitles3 = ${JSON.stringify(bookTitles)}`);
  if (params.pages) {
    let comma = (bookParams.length > 0) ? ',' : '';
    bookTitles = bookTitles + ' pages = ?' + comma;
    bookParams.push(parseInt(params.pages,10));
  }
  //console.log(`bookTitles4 = ${JSON.stringify(bookTitles)}`);
  if (params.image) {
    let comma = (bookParams.length > 0) ? ',' : '';
    bookTitles = bookTitles + ' image = ?' + comma;
    bookParams.push(params.image);
  }
  //console.log(`bookTitles = ${JSON.stringify(bookTitles)}`);
  //console.log(`bookParams = '${bookParams}'`);
  if (bookTitles && bookParams.length !== 0 && params.id) {
    bookTitles = bookTitles +' updated_at = ?';
    bookParams.push(new Date());
    bookParams.push(params.id);
    //console.log(`bookTitles = ${JSON.stringify(bookTitles)}`);
    //console.log(`bookParams = '${bookParams}'`);
    return await pool.query(`UPDATE books SET ${bookTitles} WHERE id = ?`, bookParams);
  } else {
    return { error: 'Empty book title or description or date'};
  }
};

const destroy = async (id) => {
  const bookParams  = [];
  let bookTitles = '';
  if (id) {
    bookTitles = bookTitles + ' deleted_at = ?';
    bookParams.push(new Date());
    bookParams.push(id);
    return await pool.query(`UPDATE books SET ${bookTitles} WHERE id = ?`, bookParams);
  } else {
    return { error: 'Empty book id'};
  }
};

module.exports = {
  findAll,
  findById,
  create,
  update,
  destroy
};
