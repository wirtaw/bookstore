// Update with your config settings.

const config = require('./config');

module.exports = {
  
  test: {
    client: 'mysql',
    connection: {
      database: global.gConfig.database,
      user:     global.gConfig.database_user,
      password: global.gConfig.database_password
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },

  development: {
    client: 'mysql',
    connection: {
      database: global.gConfig.database,
      user:     global.gConfig.database_user,
      password: global.gConfig.database_password
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },

  production: {
    client: 'mysql',
    connection: {
      database: global.gConfig.database,
      user:     global.gConfig.database_user,
      password: global.gConfig.database_password
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  }

};
