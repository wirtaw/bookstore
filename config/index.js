const _ = require('lodash');
require('dotenv').config();

const env = {
  mariadbUser: process.env.MARIADB_USER,
  mariadbPassword: process.env.MARIADB_PASSWORD
};

// module variables
const config = require('./config.json');
const defaultConfig = config.development;
const environment = process.env.NODE_ENV || 'development';
const environmentConfig = config[environment];
const finalConfig = _.merge(defaultConfig, environmentConfig);

// as a best practice
// all global variables should be referenced via global. syntax
// and their names should always begin with g
global.gConfig = finalConfig;

for (let key in global.gConfig) {
  if (global.gConfig.hasOwnProperty(key)) {
    if (key === 'database_user') {
      global.gConfig[key] = env.mariadbUser
    } else if (key === 'database_password') {
      global.gConfig[key] = env.mariadbPassword
    }
  }
}

// log global.gConfig
// console.log(`global.gConfig: ${JSON.stringify(global.gConfig, undefined, global.gConfig.json_indentation)}`);
