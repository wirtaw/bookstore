const request = require('supertest');
const server = require('../app.js');

const db = require('./../db/db');

beforeAll(async () => {
  // do something before anything else runs
  console.log('Jest starting!');
  await db.migrate.rollback();
  await db.migrate.latest();
  await db.seed.run();
});

// close the server after each test
afterAll(async () => {
  server.close();
  await db.migrate.rollback();
  console.log('server closed!');
});

describe('general actions', () => {
  test('returns homepage', async () => {
    const response = await request(server).get('/');
    
    expect(response.status).toBe(200);
    expect(response.text).toEqual('Welcome in the BookStore!!!');
  });
});


describe('books route tests after DB', () => {
  test('get books route GET /books without params', async () => {
    const response = await request(server).get('/books');
    
    expect(response.status).toEqual(200);
    try {
      const data = JSON.parse(response.text);
      expect(data.success).toEqual(true);
      expect(data.books.length).toEqual(0);
    } catch (e) {
    
    }
    
  });
  
  test('get books route GET /books with params', async () => {
    const response = await request(server).get('/books', {
      params: { sort: '', order: 'desc', page: 0, limit: 20 },
    });
    
    expect(response.status).toEqual(200);
    try {
      const data = JSON.parse(response.text);
      expect(data.success).toEqual(true);
      expect(data.books.length).toEqual(0);
    } catch (e) {
    
    }
    
  });
  
  test('post books route POST /books with params', async () => {
    const response = await request(server).post('/books', {
      params: { title: '', description: '', date: 0 },
    });
    
    expect(response.status).toEqual(200);
    try {
      const data = JSON.parse(response.text);
      expect(data.success).toEqual(true);
    } catch (e) {
    
    }
    
  });
  
});
