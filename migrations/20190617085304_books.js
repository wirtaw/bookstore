
exports.up = function(knex) {
  return knex.schema.createTable('books', table => {
    table.uuid('id');
    table.string('title');
    table.string('description');
    table.integer('date');
    table.integer('pages');
    table.binary('image');
    table.date('deleted_at');
    table.timestamps();
  });
};

exports.down = function(knex) {
  if (process.env.NODE_ENV !== 'production') {
    return knex.schema.dropTable('books');
  }
};
