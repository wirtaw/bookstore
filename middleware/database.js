const mysql = require('mysql');
const util = require('util');

const config = require('./../config');

const pool = mysql.createPool({
  connectionLimit: 10,
  host: 'localhost',
  user: global.gConfig.database_user,
  password: global.gConfig.database_password,
  database: global.gConfig.database
});

pool.query = util.promisify(pool.query);

pool.getConnection((err, connection) => {
  if (err) {
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      console.error('Database connection was closed.');
    }
    if (err.code === 'ER_CON_COUNT_ERROR') {
      console.error('Database has too many connections.');
    }
    if (err.code === 'ECONNREFUSED') {
      console.error('Database connection was refused.');
    }
  }
  
  if (connection) connection.release();
  
});

module.exports = pool;
