if (!process.env.NODE_ENV) {
  throw new Error('NODE_ENV not set');
}

//We don't want seeds to run in production
if (process.env.NODE_ENV === 'production') {
  throw new Error("Can't run seeds in production");
}

const faker = require('faker');
const uuidv4 = require('uuid/v4');

let randomInt = (low, high) => {
  return Math.floor(Math.random() * (high - low) + low);
};

let createRecord = (knex) => {
  return knex('books').insert({
    id: uuidv4(),
    title: faker.random.words(5),
    description: faker.lorem.paragraph(20),
    date: randomInt(1800, 2020),
    image: faker.image.image(300, 150),
    pages: randomInt(12, 1000),
    created_at: new Date(),
    updated_at: new Date(),
    deleted_at: null
  });
};

exports.seed = function(knex, Promise) {
  return knex('books').del()
    .then(function () {
      let records = [];
      
      for (let i = 1; i < 20; i++) {
        records.push(createRecord(knex, i));
      }
      
      return Promise.all(records);
    });
};
